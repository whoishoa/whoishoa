<?php 
/**
 * File: index.php
 * Created Date: 01/24/2014
 * Author: Hoa Mai
 * Description: This webpage displays your resume in a very organized and
 *    efficient manner. Edit the file located at 'private/constants.php' to
 *    update the information on your resume. 
 */
?>
<?php include 'private/constants.php'?>
<?php



?>
<!DOCTYPE html>
<html>
 <head>
 
  <title><?= TITLE ?></title>
  
  <!-- SEO Components -->
  
  
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="tritoncloud.png"/>
  
  <!-- Scripts -->
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/library.js"></script>
  
  <!-- Style -->
  <link rel='stylesheet' href='css/classic.css' />
  
 </head>
 <body>
  
 </body>
</html>