<?php 
/**
 * File: constants.php
 * Created Date: 01/24/2014
 * Author: Hoa Mai
 * Description: This file stores all of the constants for
 *    your online resume. This is the only file that needs to be
 *    edited to update your information on your resume.
 */

// The title that shows up on the web page's tab
define("TITLE", "Who is Hoa Mai?");



?>